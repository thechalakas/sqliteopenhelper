package com.thechalakas.jay.sqliteopenhelper;
/*
 * Created by jay on 30/09/17. 10:37 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity
public class User {

    @PrimaryKey
    public final int id;
    public String name;
    public int level;
    public long skillPoints;


    public User(int id, String name, long skillPoints) {
        this.id = id;
        this.name = name;
        this.skillPoints  = skillPoints;
        this.level = 0;
    }

}